using Task1;

namespace Task1Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestOwnerWithMoreFromOneCar()
        {

            Dictionary<string, string> carNumberToOwner = new Dictionary<string, string>();

            carNumberToOwner.Add("CB5839TT", "Dimitrina Dimitrova");
            carNumberToOwner.Add("CB1234TT", "Ivan Ivanov");
            carNumberToOwner.Add("CB4154HH", "Petar Ptrov");
            carNumberToOwner.Add("CB3333BB", "Stoqn Stoqnov");
            carNumberToOwner.Add("CB5555BB", "Dimitar Dimitrov");
            carNumberToOwner.Add("CB4444BB", "Stefan Stefanov");
            carNumberToOwner.Add("CB4445BB", "Stefan Stefanov");
            carNumberToOwner.Add("CB5838TT", "Dimitrina Dimitrova");

            List<string> returnCarNumber = Program.OwnerWithMoreFromOneCar(carNumberToOwner);

            Assert.IsTrue(returnCarNumber.Contains("Dimitrina Dimitrova"));
            Assert.IsTrue(returnCarNumber.Contains("Stefan Stefanov"));
        }

        [TestMethod]
        public void TestCarNumber()
        {

            Dictionary<string, string> carNumberToOwner = new Dictionary<string, string>();

            carNumberToOwner.Add("CB5839TT", "Dimitrina Dimitrova");
            carNumberToOwner.Add("CB1234TT", "Ivan Ivanov");
            carNumberToOwner.Add("CB4154HH", "Petar Ptrov");
            carNumberToOwner.Add("CB3333BB", "Stoqn Stoqnov");
            carNumberToOwner.Add("CB5555BB", "Dimitar Dimitrov");
            carNumberToOwner.Add("CB4444BB", "Stefan Stefanov");
            carNumberToOwner.Add("CB4445BB", "Stefan Stefanov");
            carNumberToOwner.Add("CB5838TT", "Dimitrina Dimitrova");

            List<string> returnCarNumber = Program.CarNumber("Dimitrina Dimitrova", carNumberToOwner);

            Assert.IsTrue(returnCarNumber.Contains("CB5838TT"));
            Assert.IsTrue(returnCarNumber.Contains("CB5839TT"));
        }
    }
}