﻿namespace Task3 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {

            string text = "texst523tklesr0est";

            string result = FindAllNonRepeatCharacters(text);
            char firstNonRepeatCharacter = FirstNonRepeatCharacter(text);

            //Console.WriteLine(firstNonRepeatCharacter);
            Console.WriteLine(result);
        }

        public static char FirstNonRepeatCharacter(string input)
        {

            int i, j;
            bool isRepeted = false;
            char[] chars = input.ToCharArray();
            for (i = 0; i < chars.Length; i++)
            {
                isRepeted = false;
                for (j = 0; j < chars.Length; j++)
                {
                    if ((i != j) && (chars[i] == chars[j]))
                    {
                        isRepeted = true;
                        break;
                    }
                }
                if (isRepeted == false)
                {
                    return chars[i];
                }
            }
            return '1';
        }

        public static string FindAllNonRepeatCharacters(string str)
        {

            int i;
            char[] chars = str.ToCharArray();
            string result = "";
            for (i = 0; i < chars.Length; i++)
            {

                if (!result.Contains(chars[i]))
                {
                    result += chars[i];
                }
            }

            return result;
        }
    }
}