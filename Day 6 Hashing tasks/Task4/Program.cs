﻿

using System.Collections.Generic;

namespace Task4 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {

            HashSet<string> set = new HashSet<string>();

            set.Add("Lorem");
            set.Add("Ipsum");
            set.Add("is");
            set.Add("simply");
            set.Add("dummy");
            set.Add("text");

            string words = "Lorem ipsum is simplly dummyy text.";

            HashSet<string> result = ReturnsAllMisspelledWords(set, words);

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

        }

        public static HashSet<string> ReturnsAllMisspelledWords(HashSet<string> misspelledWords, string words)
        {
            HashSet<string> input = new HashSet<string>();

            foreach (var word in misspelledWords)
            { 
                if (!words.Contains(word))
                {
                    input.Add(word);
                }
               
            }

            return input;
        }
    }
}