using FindClearNumber;

namespace FindClearNumberTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestFindNumber()
        {
            //Arrange
            int[] arr = new int[] { 1, 3, 2, 5, 6 };
            int expect = 4;

            //Act
            int actual =  Program.FindMissingNumber(arr);

            //Assert
            Assert.AreEqual(expect, actual);
        }

        [TestMethod]
        public void TestFindFirstMissingNumber()
        {
            //Arrange
            int[] arr = new int[] { 2, 3, 4, 5, 6, 7 };
            int expect = 1;

            //Act
            int actual = Program.FindMissingNumber(arr);

            //Assert
            Assert.AreEqual(expect, actual);
        }

        [TestMethod]
        public void TestMissingNumber()
        {
            //Arrang
            int[] arr = new int[] { 3, 2, 5, 1 };
            int expect = 4;

            //Act
            int actual = Program.FindMissingNumber(arr);

            //Assert
            Assert.AreEqual(expect, actual);
        }

        [TestMethod]
        public void TestThrowExeptionNumber()
        {
            //Arrang
            int[] arr = new int[] { };

            //Act
            //Assert
            Assert.ThrowsException<ArgumentException>(() => Program.FindMissingNumber(arr));
        }

        [TestMethod]
        public void TestThrowExeptionWithOneElement()
        {
            //Arrang
            int[] arr = new int[] { 1 };

            //Act
            //Assert
            Assert.ThrowsException<ArgumentException>(() => Program.FindMissingNumber(arr));
        }
    }
}