using FindClearNumber2;

namespace FindClearNumberTest2
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestFindNumber()
        {
            //Arrang
            int[] arr = new int[] { 1, 3, 2, 5, 6 };
            int expect = 4;

            //Act
            double actual = Program.FindMissingNumber(arr);

            //Assert
            Assert.AreEqual(expect, actual);
        }

        [TestMethod]
        public void TestMissingNumber()
        {
            //Arrang
            int[] arr = new int[] { 3, 2, 5, 1 };
            int expect = 4;

            //Act
            double actual = Program.FindMissingNumber(arr);

            //Assert
            Assert.AreEqual(expect, actual);
        }
    }
}