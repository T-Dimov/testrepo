﻿using System.Net.NetworkInformation;

namespace SmallTasks
{

    public class ReverseString
    {
        public static void Main() { }

        public static string Reverse(string inputString)
        {
            string result = "";

            for (int i = inputString.Length -1; i >= 0; i--)
            {
                result += inputString[i];
            }

            return result;
        }

        
 
    }

}

