using ConsoleApp1;

namespace TestFiboncci
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestWithTrueArray()
        {
            int [,] input  = {
                { 1, 1, 2, 3 },
                { 5, 8, 13, 21 },
                { 34, 55, 89, 144 },
                { 233, 377, 610, 987 } };

            bool expect = true;

            bool actualResult = Program.AreAllFibonacci(input);

            Assert.AreEqual(actualResult, expect);
        }

        [TestMethod]
        public void TestWithFalseArray()
        {
            int[,] input = { { 1, 2, 3, 4 },
                      { 5, 6, 7, 8 },
                      { 1, 2, 3, 4 },
                      { 5, 6, 7, 8 } };

            bool expect = false;

            bool actualResult = Program.AreAllFibonacci(input);

            Assert.AreEqual(actualResult, expect);
        }

        [TestMethod]
        public void TestWithEmptyArray()
        {
            int[,] input = { {  } };

            bool expect = false;

            bool actualResult = Program.AreAllFibonacci(input);

            Assert.AreEqual(actualResult, expect);
        }

        [TestMethod]
        public void TestWithRandom()
        {
            int[,] input = {
                { 1, 1, 2, 3 },
                { 21, 8, 18, 21 } };

            bool expect = true;

            bool actualResult = Program.AreAllFibonacci(input);

            Assert.AreEqual(actualResult, expect);
        }
    }
}