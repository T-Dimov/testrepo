﻿using System.Xml.Linq;

namespace Task3
{
    public class Program
    {
        static void Main(string[] args)
        {
            var node76 = new BinaryTree(76, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node35 = new BinaryTree(35, null, null);
            var node69 = new BinaryTree(69, node61, node76);
            var node38 = new BinaryTree(38, node35, null);
            var node24 = new BinaryTree(24, null, null);
            var node11 = new BinaryTree(11, null, node24);
            var node4 = new BinaryTree(4, node38, node69);
            var root = new BinaryTree(25, node11, node4);


            int heightTree = HeightTree(root);

            Console.WriteLine(heightTree);

        }

        public static int HeightTree(BinaryTree node)
        {

            if (node == null)
                return 1;


            if (node.Left != null && maxValue(node.Left) > node.Value)
            {
                return 0;
            }
                

            if (node.Right != null && minValue(node.Right) < node.Value)
            {
                return 0;
            }
               
            if (HeightTree(node.Left) == 0 || HeightTree(node.Left) == 0)
            {
                return 0;
            }

            return 1;
        }

        static int maxValue(BinaryTree node)
        {
            if (node == null)
            {
                return Int16.MinValue;
            }
            int value = node.Value;
            int leftMax = maxValue(node.Left);
            int rightMax = maxValue(node.Right);

            return Math.Max(value, Math.Max(leftMax, rightMax));
        }

        static int minValue(BinaryTree node)
        {
            if (node == null)
            {
                return Int16.MaxValue;
            }
            int value = node.Value;
            int leftMax = minValue(node.Left);
            int rightMax = minValue(node.Right);

            return Math.Min(value, Math.Min(leftMax, rightMax));
        }
    }
}